# Summary

- [Stack and Heap Allocation](./stack_and_heap_allocation.md)

- [Dangling Pointers and Memory Leaks](./memory_leaks.md)

- [RAII Pattern](./raii_pattern.md)

- [The STL](./stl.md)
